class MarvelWhois < Rubot::Commands::Base
  # listen /who is (.*)$/

  def answer(*name)
    name = name.first

    channel.send_message("Looking for #{name}")

    time_stamp = Time.now.to_f.to_s
    hash = Digest::MD5.hexdigest time_stamp + Rubot.config.marvel.private_key + Rubot.config.marvel.public_key

    base_url = URI.parse 'http://gateway.marvel.com/v1/public/characters'
    base_url.query = URI.encode_www_form({ts: time_stamp, apikey: Rubot.config.marvel.public_key, hash: hash, limit: 1, nameStartsWith: name.strip})

    http_get(base_url)

    response = Net::HTTP.get base_url
  end

  private

  def response_success(request)
    data = JSON.parse(request.response)

    if data['data']['total'].zero?
      channel.send_message "Não encontrei nada sobre '#{name}'"
    else
      channel.send_message data['data']['results'].first['name']
      channel.send_message data['data']['results'].first['description']
      channel.send_message data['data']['results'].first['thumbnail']['path'] + '.' + data['data']['results'].first['thumbnail']['extension']
      channel.send_message "done :wink:"
    end
  end

  def response_fail(error)
    channel.send_message('Sorry, an error ocurred!')
  end
end