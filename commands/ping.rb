class Ping < Rubot::Commands::Base
  listen /^ping$/

  def answer
    channel.send_message "pong"
  end

end