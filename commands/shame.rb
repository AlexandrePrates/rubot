class Shame < Rubot::Commands::Base
  listen /^[Ss]hame/, on_channel: 'hall_of_shame'

  def answer
    channel.send_attachment({
          title: 'Shame! :bell:  :bell:  :bell: ',
          image_url: 'http://www.relatably.com/m/img/shame-memes/image.png',
          color: '#F35A00'
      })

  end

end