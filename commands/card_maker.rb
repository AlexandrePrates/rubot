class CardMaker < Rubot::Commands::Base
  listen /card of (.+)$/

  def answer(name)
    make_card(name)
  end

  private

  def make_card(twitter_user_name)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key    = Rubot.config.card_maker.twitter_key
      config.consumer_secret = Rubot.config.card_maker.twitter_secret
    end

    begin
      user = client.user(twitter_user_name).attrs
    rescue
      channel.send_message "Can't found twitter user #{twitter_user_name}"
      return false
    end

    # puts user[:profile_image_url].gsub('_normal.', '.')
    card_data = {
      "name"=> user[:name],
      "color"=> %w{White Blue Black Red Green Gold}.sample,
      "mana_r"=>"0",
      "mana_u"=>"0",
      "mana_g"=>"0",
      "mana_b"=>"0",
      "mana_w"=>"0",
      "mana_colorless"=>"1",
      "picture"=> user[:profile_image_url].gsub('_normal.', '.'),
      "supertype"=> %w{Legendary Basic Snow World Outgoing}.sample,
      "cardtype"=> ['', 'Creature', 'Planeswalker'].sample,
      "subtype"=> user[:screen_name],
      "expansion"=> %w{Alpha Beta Unlimited Revised}.sample,
      "rarity"=> %w{Common Uncommon Rare Mythic}.sample,
      "cardtext"=> user[:description],
      "power"=>"1",
      "toughness"=>"1",
      "artist"=>"www.mtgcardmaker.com",
      "bottom"=>"™ & © 2013 TwitterGather @ ajfprates",
      "set1"=>"1",
      "set2"=>"1",
      "setname"=>"TwitterGather"}

    url = URI 'http://www.mtgcardmaker.com/mcmaker/createcard.php'
    url.query = URI.encode_www_form card_data
    channel.send_message url.to_s
  end

end