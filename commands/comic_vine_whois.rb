class ComicVineWhois < Rubot::Commands::Base
  listen /who is (.*)$/

  def answer(*name)
    name = name.first
    channel.send_message("Looking for #{name}")

    url = URI.parse Rubot.config.comicvine.search_url
    filter = "name:#{name}"
    url.query = URI.encode_www_form({api_key: Rubot.config.comicvine.api_key, limit: 1, filter: filter, format: 'json', field_list: 'name,real_name,birth,gender,deck,image'})

    http_get(url)
  end

  def response_success(request)
    data = JSON.parse request.response
    format_response data
  end

  def response_fail(error)
    channel.send_message("Sorry, an error ocurred!")
  end

  private

  def format_response(data)
    if data['number_of_total_results'] > 0
      info = data['results'].first
      channel.send_attachment({
            title: "#{info['name']}",
            fallback: info['deck'],
            fields: [
                {title: 'Real name', value: info['real_name'], short: false},
                {title: 'Birth', value: info['birth'], short: false},
                {title: 'Gender', value: gender_for(info['gender']), short: false},
            ],
            text: info['deck'],
            image_url: Rubot.config.comicvine.static_host + small_image_path(data),
            color: '#F35A00'
        })
    else
      channel.send_message "I found nothing :sob:"
    end
  end

  def gender_for(id)
    case id
    when 1
      'male'
    when 2
      'female'
    else
      'undefined'
    end
  end

  def small_image_path(data)
    URI.escape info['image']['small_url'] rescue ''
  end
end