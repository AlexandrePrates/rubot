require 'bundler'
Bundler.require :default, :development

require 'uri'
require 'cgi'

module Rubot
  include Configureasy
  load_config :config
  load_config :slack

  extend self

  def run
    EventMachine.epoll
    EventMachine.run do
      trap("TERM") { stop }
      trap("INT")  { stop }

      fetch_data do |response|
        data = JSON.parse response
        Rubot::Slack.parse data
        Rubot::WebSocketClient.connect data['url']
      end
    end
  rescue Exception => e
    puts e.message
    puts e.backtrace
  end

  private

  def fetch_data(&callback)
    url = URI.parse slack.rtm_url
    url.query = URI.encode_www_form(token: slack.token)

    request = EventMachine::HttpRequest.new(url).get(redirects: 1)
    request.callback do
      callback.call request.response
    end
  end

end

require './lib/rubot/slack'
require './lib/rubot/slack/channel'
require './lib/rubot/slack/message'
require './lib/rubot/slack/user'
require './lib/rubot/handler'
require './lib/rubot/commands'
require './lib/rubot/send'
require './lib/rubot/web_socket_client'

Dir['./commands/*.rb'].each do |file|
  puts "Loading #{file}"
  require file
end
