module Rubot::WebSocketClient
  extend self

  def connect(url)
    @ws = WebSocket::EventMachine::Client.connect(:uri => url);
    @ws.onopen    &Rubot::WebSocketClient.method(:onopen)
    @ws.onmessage &Rubot::WebSocketClient.method(:onmessage)
    @ws.onclose   &Rubot::WebSocketClient.method(:onclose)
    @ws.onerror   &Rubot::WebSocketClient.method(:onerror)
    @ws.onping    &Rubot::WebSocketClient.method(:onping)
    @ws.onpong    &Rubot::WebSocketClient.method(:onpong)
  end

  def onopen(i)
    puts "Connected to Slack"
  end

  def onmessage(message, type)
    Rubot::Commands.process Rubot::Slack::Message.new(message)
  end

  def onclose(event, reason)
    puts "Disconnected"
  end

  def onerror(error)
    puts "[ERROR] #{error}"
  end

  def onping(*attr)
    # puts "Receied ping: #{attr.inspect}"
  end

  def onpong
    # puts "Received pong: #{msg}"
  end

  def stop
    puts "Terminating connection"
    EventMachine.stop
  end
end