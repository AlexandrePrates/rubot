module Rubot::Send
  extend self

  def message(channel, message)
    @send_url ||= URI.parse 'https://slack.com/api/chat.postMessage'
    params = {token: Rubot.slack.token, channel: channel.id, text: message, as_user: true}
    @send_url.query = params_to_query(params)
    EventMachine::HttpRequest.new(@send_url.to_s).get
  end

  def attachment(channel, params = {})
    attachments = [params].to_json
    @send_url ||= URI.parse 'https://slack.com/api/chat.postMessage'
    params = {token: Rubot.slack.token, channel: channel.id, attachments: attachments, as_user: true}
    @send_url.query = params_to_query(params)
    EventMachine::HttpRequest.new(@send_url.to_s).get
  end

  private

  def post_message_url
    @url ||= URI.parse 'https://slack.com/api/chat.postMessage'
    @url.clone
  end

  def params_to_query(params = {})
    URI.encode_www_form params
  end

end