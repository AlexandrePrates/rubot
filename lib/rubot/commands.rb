module Rubot::Commands
  class << self
    attr_reader :listeners

    def add_listener(pattern, channel, klass)
      @listeners ||= []
      callback = lambda { |message, values| klass.new(message).answer(*values) }
      @listeners << Rubot::Handler.new(pattern, channel, callback)
    end

    def process(message)
      self.listeners.each { |handler| handler.run(message) if handler.match?(message) }
    end

  end

  class Base
    class << self
      def listen(pattern, options = {})
        options[:on_channel] ||= :all
        Rubot::Commands.add_listener(pattern, options[:on_channel], self)
      end
    end

    attr_reader :message, :channel, :user

    def initialize(message)
      p message
      @message = message
      @channel = message.channel
      @user = message.user
    end

    def answer(*matches)
      p [:answer, matches]
    end

    private

    def response_success(request)
      p [:response_success, request]
    end

    def response_fail(error)
      p [:response_fail, error]
    end

    def http_get(url)
      http = EventMachine::HttpRequest.new(url).get(redirects: 5)
      http.errback { |error| self.response_fail error }
      http.callback { |request| self.response_success request }
    end

    def http_post(url, body = {})
      http = EventMachine::HttpRequest.new(url).get(redirects: 5, body: body)
      http.errback { |error| self.response_fail error }
      http.callback { |request| self.response_success request }
    end

  end
end
