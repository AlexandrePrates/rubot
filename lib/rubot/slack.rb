module Rubot
  module Slack
    extend self

    def parse(data)
      data['channels'].each { |channel| Rubot::Slack::Channel.new channel unless channel['is_archived'] }
      data['users'].each { |user| Rubot::Slack::User.new user unless user['deleted'] }
      Rubot::Slack::User.my_id = data['self']['id']
    end
  end
end