class Rubot::Slack::User
  class << self
    attr_accessor :my_id

    def me
      self[my_id]
    end

    def [](id)
      self.users.find { |channel| channel.id == id }
    end

    def users
      @@users ||= []
    end
  end

  attr_reader :id, :name, :real_name, :image

  def initialize(data)
    @id = data['id']
    @name = data['name']
    @real_name = data['real_name']
    @image = data['profile']['image_72']
    self.class.users << self
  end

  def me?
    self == Rubot::Slack::User.me
  end
end