class Rubot::Slack::Message < OpenStruct

  def initialize(message)
    hash = JSON.parse(message)
    super hash
  end

  def channel
    @channel ||= (Rubot::Slack::Channel[@table[:channel]] || OpenStruct.new)
  end

  def user
    @user ||= (Rubot::Slack::User[@table[:user]] || OpenStruct.new)
  end

  def message?
    self.type == 'message'
  end

  def match(regexp)
    regexp.match self.text
  end

end