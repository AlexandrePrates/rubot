class Rubot::Slack::Channel
  class << self

    def [](text)
      self.channels.find { |channel| channel.id == text || channel.name == text }
    end

    def channels
      @@channels ||= []
    end
  end

  attr_reader :id, :name, :is_member

  def initialize(data)
    @id = data['id']
    @name = data['name']
    @is_member = data['is_member']
    self.class.channels << self
  end

  def member?
    !!@is_member
  end

  def send_message(message)
    Rubot::Send.message self, message
  end

  def send_attachment(params = {})
    Rubot::Send.attachment self, params
  end

end