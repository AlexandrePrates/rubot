class Rubot::Handler
  attr_reader :regexp, :channel, :callback

  def initialize(regexp, channel, callback)
    @regexp = regexp
    @channel = channel
    @callback = callback
  end


  def match?(message)
    valid_message?(message) && valid_channel?(message)
  end

  def run(message)
    values = regexp.match(message.text).captures
    callback.call(message, values)
  end

  private

  def valid_message?(message)
    message.message? && !message.user.me? && message.match(regexp)
  end

  def valid_channel?(message)
    message.channel.member? && (channel == :all || message.channel.name == channel)
  end

end
