#!/bin/env ruby

require 'bundler'
Bundler.require :default, :development

require 'cgi'
require 'json'
require 'ostruct'
require 'digest/md5'
require 'twitter'

# SLACK_TOKEN = 'xoxb-4906392166-3oFh9KhcvMpmPEx7E503ZLfX'
SLACK_TOKEN = 'IUAKFSIXBOf6gaNlVGZejZgw'

#lab
CHANNEL = 'automation'

#lab
# CHANNEL = 'C04TC3SAP'

#Senzala
# CHANNEL = 'C0326TM5C'

ME = 'U04SNBJ4W'

def slack_data(&callback)
  url = URI.parse 'https://slack.com/api/rtm.start'
  url.query = URI.encode_www_form(token: SLACK_TOKEN, pretty: true)

  request = EventMachine::HttpRequest.new(url).get(redirects: 1)
  request.callback do
    p request.response
    callback.call request.response
  end
end

def send_message(text)
  @send_url ||= URI.parse 'https://slack.com/api/chat.postMessage'
  @send_url.query = URI.encode_www_form({token: SLACK_TOKEN, channel: CHANNEL, text: text, as_user: true})
  # EventMachine::HttpRequest.new(@send_url.to_s).get
end

def parse(msg)
  OpenStruct.new JSON.parse(msg)
end

def send_attachment(data = {})
  attachments = [data].to_json
  @send_url ||= URI.parse 'https://slack.com/api/chat.postMessage'
  @send_url.query = URI.encode_www_form({token: SLACK_TOKEN, channel: CHANNEL, attachments: attachments, as_user: true})
  # EventMachine::HttpRequest.new(@send_url.to_s).get
end

def whois_comicvine(name)
  send_message("Looking for #{name}")
  apikey = 'b73ecc0ad9eac26d7ebdb4f3c3b80f8b38a625b6'
  url = URI.parse 'https://www.comicvine.com/api/characters'
  filter = "name:#{name}"
  url.query = URI.encode_www_form({api_key: apikey, limit: 1, filter: filter, format: 'json', field_list: 'name,real_name,birth,gender,deck,image'})
  request = EventMachine::HttpRequest.new(url).get(redirects: 4)
  request.callback { show_info(request.response) }
  request.errback { |error| p request }

  def gender_for(id)
    case id
    when 1
      'male'
    when 2
      'female'
    else
      'undefined'
    end
  end

  def show_info(response)
    data = JSON.parse response

    if data['number_of_total_results'] > 0
      info = data['results'].first
      pp info
      send_attachment({
            title: "#{info['name']}",
            fallback: info['deck'],
            fields: [
                {title: 'Real name', value: info['real_name'], short: false},
                {title: 'Birth', value: info['birth'], short: false},
                {title: 'Gender', value: gender_for(info['gender']), short: false},
            ],
            text: info['deck'],
            image_url: "http://static.comicvine.com#{URI.escape info['image']['small_url'] rescue ''}",
            color: '#F35A00'
        })
    else
      send_message "I found nothing :sob:"
    end
  end
end

def whois_marvel(name)
  send_message("Looking for #{name}")
  public_api = "c016a95766eee9faae10a269236a4e5c"
  private_api = "9a59414ba0b0ff8672f85adb0ca3c9e4bce929ac"
  time_stamp = Time.now.to_f.to_s

  hash = Digest::MD5.hexdigest time_stamp + private_api + public_api

  base_url = URI.parse 'http://gateway.marvel.com/v1/public/characters'
  base_url.query = URI.encode_www_form({ts: time_stamp, apikey: public_api, hash: hash, limit: 1, nameStartsWith: name.strip})

  response = Net::HTTP.get base_url

  data = JSON.parse(response)

  p [:api_return, data['data']]
  if data['data']['total'].zero?
    send_message "Não encontrei nada sobre '#{name}'"
  else
    send_message data['data']['results'].first['name']
    send_message data['data']['results'].first['description']
    send_message data['data']['results'].first['thumbnail']['path'] + '.' + data['data']['results'].first['thumbnail']['extension']
    send_message "done :wink:"
  end
end


def make_card(twitter_user_name)
  client = Twitter::REST::Client.new do |config|
    config.consumer_key    = "ilMAGp8Kpfc1LXHHrB9WWG7Wo"
    config.consumer_secret = "5Tm3l59VF7a8cjmEX6a44PpLUUr9cCWaiLp3JHUJ3ilfn5DfTh"
  end

  begin
    user = client.user(twitter_user_name).attrs
  rescue
    send_message "Can't found twitter user #{twitter_user_name}"
    return false
  end

  puts user[:profile_image_url].gsub('_normal.', '.')

  card_data = {
    "name"=> user[:name],
    "color"=> %w{White Blue Black Red Green Gold}.sample,
    "mana_r"=>"0",
    "mana_u"=>"0",
    "mana_g"=>"0",
    "mana_b"=>"0",
    "mana_w"=>"0",
    "mana_colorless"=>"1",
    "picture"=> user[:profile_image_url].gsub('_normal.', '.'),
    "supertype"=> %w{Legendary Basic Snow World Outgoing}.sample,
    "cardtype"=> ['', 'Creature', 'Planeswalker'].sample,
    "subtype"=> user[:screen_name],
    "expansion"=> %w{Alpha Beta Unlimited Revised}.sample,
    "rarity"=> %w{Common Uncommon Rare Mythic}.sample,
    "cardtext"=> user[:description],
    "power"=>"1",
    "toughness"=>"1",
    "artist"=>"www.mtgcardmaker.com",
    "bottom"=>"™ & © 2013 TwitterGather @ ajfprates",
    "set1"=>"1",
    "set2"=>"1",
    "setname"=>"TwitterGather"}

  url = URI 'http://www.mtgcardmaker.com/mcmaker/createcard.php'
  url.query = URI.encode_www_form card_data

  send_message url.to_s
end

def start_ws_client(uri)
  @ws = WebSocket::EventMachine::Client.connect(:uri => uri);

  @ws.onopen do
    puts "Connected"
    send_message("I'm in!")
  end

  @ws.onmessage do |msg, type|
    puts "Received message: #{msg}"
    process(msg)
  end

  @ws.onclose do
    puts "Disconnected"
  end
end

def creyssonize(message)
  Bundler.with_clean_env do
    Dir.chdir "../Stemmer" do
      message = %x{bundle exec ./creysson "#{message}"}
    end
  end
  send_message message.chomp
end

def process(msg)
  message = parse msg
  return if !(message.type == 'message' && message.channel == CHANNEL && message.user != ME)

  text = message.text || ''

  case text.downcase
  when /creyssonize (.*)/
    creyssonize($1)
  when /who is botmon$/
    send_message('Is a ordinary BOT for Slack')
  when /who is (.*)$/
    whois_comicvine($1.strip)
  when /<@U04SNBJ4W>: (olá|oi|ola|fala)/
    response = ["Olá", "Oi", "Eu", ":grin:"].sample
    send_message response
  when /card of (.*)$/
    make_card $1.strip
  when /ping/
    send_message('pong')
  end
rescue Exception => e
  puts "\n\nERROR: #{e.message}\n#{caller}\n\n"
  send_message "Sorry something is wrong :astonished:"
end

def stop
  puts "Terminating connection"
  EventMachine.stop
end

EM.epoll
EM.run do
  trap("TERM") { stop }
  trap("INT")  { stop }

  slack_data do |response|
    data = JSON.parse response
    start_ws_client data['url']
  end
end